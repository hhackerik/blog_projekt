<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/**
 * index file
 */

ini_set('display_errors', '1');
// Starting sessions
session_start();


/** Automatic class loader function*/
function autoload($class)
{
    $url = parse_url("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $help_parsed_url = explode("/",$url['path']);

    $key_number = 0;

    for ($i = 0; $i < count($help_parsed_url); $i++)
    {
        if(!empty($help_parsed_url[$i]))
        {
            $parsed_url[$key_number] = ucfirst($help_parsed_url[$i]);
            $key_number++;
        }
    }


    list($className) = explode("Controller", $class);


    //Check if class name ending Controller
    if ((mb_strlen($class) >= mb_strlen("Controller")) && (mb_strpos($class, "Controller", mb_strlen($class) - mb_strlen("Controller"))) !== false)
    {

        if (file_exists('controllers/' . $class. '.php'))
	    {
	        require("controllers/" . $class . ".php");
	    }
        elseif(file_exists('modules/'.$parsed_url[0].'/controller/'.$class .'.php'))
	    {

            if(file_exists('modules/'.$parsed_url[0].'/controller/'.$class.'.php'))
	        {
                require('modules/'.$parsed_url[0].'/controller/'.$class.'.php');
	        }
            else
            {

		        require('modules/'.$className.'/controller/'.$class.'.php');
            }
        }
    }
    //Else if check if class exist in models
    else
    {

        if (file_exists("models/".$class.".php"))
        {
             require("models/".$class.".php");
        }

        elseif (file_exists('modules/'.$parsed_url[0].'/model/'.$class.'.php'))
        {

            require('modules/'.$parsed_url[0].'/model/'.$class.'.php');
        }
       
    }

}


// Callback register
spl_autoload_register("autoload");

// Include settings file
require "./inc/config.php";


// init RouterController()
$router = new RouterController();
$router->run();

// Render head layout
$router->renderView();