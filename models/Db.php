<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */

/**
 * Class Db is PDO database wrapper
 */
class Db {

    /** @var string $connect database connect*/
    private static $connect;


    /** @var array $settings database settings*/
    private static $settings = Array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    );

	/** method connect() - Connect to dabase*/
    public static function connect($host, $user, $password, $database)
    {

        self::$connect = @new PDO(
                "mysql:host=$host;dbname=$database",
                $user,
                $password,
                self::$settings
            );
        
    }

    /** Get one row
     * @return array
     */
    public static function getOneRow($query, $parametres = Array())
    {
        $conn = self::$connect->prepare($query);
        $conn->execute($parametres);
        return $conn->fetch();
    }

    /** Get all rows
     * @return array
     */
    public static function getAllRows($query, $parametres = Array())
    {
        $conn = self::$connect->prepare($query);
        $conn->execute($parametres);
        return $conn->fetchAll();
    }



	/** Call simple query
     * @return int number of affected rows
     */
    public static function query($query, $parametres = Array())
    {
        $conn = self::$connect->prepare($query);
        $conn->execute($parametres);
        return $conn->rowCount();
    }

    /** Delete from database
     * @return int number of affected rows
     */
    public static function delete($query, $parametres = Array())
    {
        $conn = self::$connect->prepare($query);
        $conn->execute($parametres);
        return $conn->rowCount();
    }



    /** Insert row to database
     * @var string $table - name of table
     * @var array $parametres - associative array where key is column name and value is value which we can insert to db
     * @return int number of affected rows
     */
    public static function insert($table, $parametres = Array())
    {
        return self::query("INSERT INTO `$table` (`". implode('`, `', array_keys($parametres)). "`) VALUES (".str_repeat('?,', sizeOf($parametres)-1)."?)", array_values($parametres));
    }

    /** Call simple query
     * @var string $table - name of table
     * @var array $parametres - associative array where key is column name and value is value which we can edit in db
     * @return int number of affected rows
     */
    public static function edit($table, $parametres = Array(), $condition, $parametry = Array())
    {
            return self::query("UPDATE `$table` SET `". implode('` = ?, `', array_keys($parametres)). "` = ? " . $condition, array_merge(array_values($parametres), $parametry));
    }
	
}