<?php
/**
* @author Viktor Beránek
* @author Viktor Beránek <hhackerik@me.com>
*/


/** Homepage model
*/

class HomepageModel
{
    /**Return all articles if is user logged show articles which are just for logged users
     * @return Array()
    */
    function getAllArticles()
    {
        //If user is logged we show him non public articles

        $public_articles = ($_SESSION['user']['is_logged'])? array("1,0"): array(1);

        return Db::getAllRows("SELECT a.id, a.name, a.description, a.photo, a.is_public, a.created, u.name as user_name, u.surname as user_surname
                                FROM articles as a
                                JOIN users as u on u.id = a.users_id
                                WHERE a.is_public IN ({$public_articles[0]})
                                 ORDER BY id DESC
                                ",$public_articles);
    }
}