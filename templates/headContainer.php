<div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">

        <h1 class="page-header">
            Blog

        </h1>
        <?if($this->all_articles):?>
            <?foreach($this->all_articles as $aK => $aV):?>
                <h2>
                    <a href="/article/detail/?id=<?=$aV['id']?>"><?=$aV['name']?></a>
                </h2>
                <p class="lead">
                    by <?=$aV['user_name']?> <?=$aV['user_surname']?>
                </p>

                <p><span class="glyphicon glyphicon-time"></span> Publikováno <?=DateTime::createFromFormat("Y-m-d H:i:s", $aV['created'])->format("d.m.Y H:i")?></p>


                <?if($aV['photo']):?>
                    <hr>
                        <img width="304" height="236" class="img-thumbnail" src="../img/upload/<?=$aV['photo']?>"/>
                    <hr>
                <?endif?>

                <p><?if(strlen($aV['description']) >= 150):?>
                        <?=substr($aV['description'],0,150)?>..
                    <?else:?>
                        <?=$aV['description']?>
                    <?endif?>
                </p>
                <a class="btn btn-primary" href="/article/detail/?id=<?=$aV['id']?>">Více<span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

            <?endforeach?>
        <?else:?>
        <?endif?>
        <!-- First Blog Post -->





    </div>



</div>
<!-- /.row -->