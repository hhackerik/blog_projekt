<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Viktor Beránek">
    <title>Blog</title>

    <link rel="stylesheet" href="http://sheepsoft.cz/css/bootstrap.css" />

</head>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Blog</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">Články</a>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right" id="main-navigation">

                <?if($_SESSION['user']['is_logged']):?>

                    <li>
                        <span>Přihlášen <?=$_SESSION['user']['name']?> <?=$_SESSION['user']['surname']?></span>
                    </li>
                    <?if($_SESSION['user']['role'] == 1):?>

                        <li>
                            <a href="/users">Správa uživatelů</a>
                        </li>
                        <li>
                            <a href="/article">Články</a>
                        </li>
                <?endif?>
                        <li>
                            <a href="/?logout=1">Odhlásit</a>
                        </li>
                <?else:?>
                    <form class="form-inline" method="POST" role="form">
                        
                        <div class="form-group">
                          
                          <input type="email" class="form-control" name="login[email]" placeholder="email" id="email">
                        </div>
                        <div class="form-group">
                        
                          <input type="password" class="form-control" name="login[pswd]" placeholder="heslo" id="pwd">
                        </div>
                       
                        <button type="submit" name="singup" class="btn btn-default">Přihlásit</button>
                    </form>
                <?endif?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    
    <div class="row">
        
        <?php $this->controller->renderView();?>
    </div>   
</div>
    <hr>
        

    <!-- Footer -->
    

<!-- /.container -->

<!-- jQuery -->
<script src="http://sheepsoft.cz/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="http://sheepsoft.cz/js/bootstrap.min.js"></script>

<!--[if lte IE 8]>
<script src="http://sheepsoft.cz/js/html5.js" type="text/javascript"></script>
<![endif]-->


</body>
</html>