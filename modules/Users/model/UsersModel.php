<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Users model
 */

class UsersModel
{
    /**Return all users s
     * @return Array()
     */
    function getAllUsers()
    {
        return Db::getAllRows("SELECT u.id, u.email, u.name, u.surname, u.role
                                FROM users as u");

    }
}