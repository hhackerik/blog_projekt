<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Users controller for admin users
 */

class UsersController extends Controller
{
    public function run()
    {

        $users_model = new UsersModel();

        if($_GET['article_delete'])
        {
            Db::delete("articles", "WHERE id = '{$_GET[article_delete]}'");
            Header("Location: /articles/");
        }


        $this->users = $users_model->getAllUsers();

     

        $this->folder = "Users";

        $this->template = "Users";
    }
}
?>
        