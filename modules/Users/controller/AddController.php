<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Add users controller for admin users
 */
class AddController extends Controller
{
    public function run()
    {

        if(isset($_POST['add']))
        {
            try
            {
                if($_POST['data']['name'] AND $_POST['data']['surname'] AND $_POST['data']['email'] AND $_POST['data']['password'])
                {
                    $data = $_POST['data'];
                    $data['password'] = md5($data['password']);
                    Db::insert("users", $data);
                    throw new Exception("Uživatel úspešně vytvořen");
                }
                else
                {
                    throw new Exception("Zadejte všechny údaje");
                }
            }
            catch(Exception $e)
            {
                $this->message = $e->getMessage();
            }
        }

        $this->folder = "Users";

        $this->template = "Add";
    }
}
?>
        