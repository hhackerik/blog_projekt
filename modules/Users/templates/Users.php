<div class="row">
    <?if($_SESSION['user']['is_logged'] AND $_SESSION['user']['role'] == 1):?>
        <p><a href="/users/add/">Přidat uživatele</a>
        <?if($this->users):?>
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>

                        <th>
                            Email
                        </th>

                        <th>
                            Jméno
                        </th>
                        <th>
                            Administrator
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($this->users as $uK => $uV):?>
                        <tr>
                            <td>
                                <?=$uV['id']?>
                            </td>

                            <td>
                                <?=$uV['email']?>
                            </td>

                            <td>
                                <?=$uV['name']?> <?=$uV['surname']?>
                            </td>
                            <td>
                                <?if($uV['role'] == 1):?><img  width="20" height="20" src="../../img/true.png"/><?else:?><img  width="20" height="20" src="../../img/false.png"/><?endif?>
                            </td>
                        </tr>
                    <?endforeach?>
                </tbody>

            </table>
        <?else:?>
            <p>Neexistuje žádný uživatel</p>
        <?endif?>
    <?else:?>
        <p>Na tuto stránku nemáte oprávnění</p>
    <?endif?>


</div>
<!-- /.row -->