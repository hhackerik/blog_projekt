    <?if($_SESSION['user']['is_logged'] AND $_SESSION['user']['role'] == 1):?>
        <?if($this->message):?><p><?=$this->message?></p><?endif?>
        <form method="POST" class="form-horizontal">
            <fieldset>

                <!-- Form Name -->
                <legend>Přidání uživatele</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="data[name]">Jméno</label>
                    <div class="col-md-4">
                        <input id="data[name]" name="data[name]" type="text" placeholder="" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="data[surname]">Příjmení</label>
                    <div class="col-md-4">
                        <input id="data[surname]" name="data[surname]" type="text" placeholder="" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="data[email]">Email</label>
                    <div class="col-md-4">
                        <input id="data[email]" name="data[email]" type="text" placeholder="" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="data[password]">Heslo</label>
                    <div class="col-md-4">
                        <input id="data[password]" name="data[password]" type="password" placeholder="" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Multiple Checkboxes (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="data[role]">Admin</label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="data[role]-0">
                            <input type="checkbox" name="data[role]" id="data[role]-0" value="1">
                            Ano
                        </label>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="add"></label>
                    <div class="col-md-4">
                        <button id="add" name="add" class="btn btn-primary">Přidat</button>
                    </div>
                </div>

            </fieldset>
        </form>


    <?else:?>
        <p>Na tuto stránku nemáte oprávnění</p>
    <?endif?>

<!-- /.row -->