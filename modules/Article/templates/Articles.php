
    <?if($_SESSION['user']['is_logged'] AND $_SESSION['user']['role'] == 1):?>
        <p><a href="/article/add">Přidat článek</a></p>
        <?if($this->data):?>
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Jméno
                        </th>
                        <th>
                            Vytvořil
                        </th>
                        <th>
                            Datum
                        </th>
                        <th>
                            Veřejná novinka
                        </th>
                        <th colspan="2">
                            Úpravy
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($this->data as $dK => $dV):?>
                        <tr>
                            <td>
                                <?=$dV['id']?>
                            </td>

                            <td>
                                <?=$dV['name']?>
                            </td>

                            <td>
                                <?=$dV['user_name']?>  <?=$dV['user_surname']?>
                            </td>

                            <td>
                                <?=DateTime::createFromFormat("Y-m-d H:i:s", $dV['created'])->format("d.m.Y H:i")?>
                            </td>

                            <td>
                                <?if($dV['is_public']):?><img  width="20" height="20" src="../../img/true.png"/><?else:?><img  width="20" height="20" src="../../img/false.png"/><?endif?>
                            </td>
                            <td colspan="2">
                                <a href="/article/edit/?id=<?=$dV['id']?>">Editace</a> -  <a href="/article/?delete_id=<?=$dV['id']?>">Smazat</a>
                            </td>
                        </tr>
                    <?endforeach?>
                </tbody>
            </table>


        <?else:?>
            <p>Žádný článek v databázi</p>
        <?endif?>
    <?else:?>
        <p>Na tuto stránku nemáte oprávnění</p>
    <?endif?>
