<?if($_SESSION['user']['is_logged'] AND $_SESSION['user']['role'] == 1 AND $this->data):?>
    <?if($this->message):?><p><?=$this->message?></p><?endif?>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend>Editace článku</legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="data[name]">Název</label>
                <div class="col-md-4">
                    <input id="data[name]" name="data[name]" type="text" placeholder=""  value="<?=$this->data['name']?>" class="form-control input-md">

                </div>
            </div>

            <!-- File Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="image">Obrázek k článku</label>
                <div class="col-md-4">
                    <input id="image" name="image" class="input-file" type="file">
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="data[description]">Článek</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="textarea" height = "400px"  name="data[description]"><?=$this->data['description']?></textarea>
                </div>
            </div>

            <!-- Multiple Checkboxes (inline) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="data[is_public]">Veřejný článek pro nepřihlášené uživatele</label>
                <div class="col-md-4">
                    <label class="checkbox-inline" for="data[is_public]-0">
                        <input type="checkbox" name="data[is_public]" id="data[is_public]-0" value="1" <?if($this->data['is_public']):?>checked<?endif?>>
                        Ano
                    </label>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="edit"></label>
                <div class="col-md-4">
                    <button id="add" name="edit" class="btn btn-primary">Upravit</button>
                </div>
            </div>

        </fieldset>
    </form>

<?else:?>
    <p>Na tuto stránku nemáte oprávnění</p>
<?endif?>
<!-- /.row -->