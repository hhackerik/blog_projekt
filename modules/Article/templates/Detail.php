<div class="row">
    <?if($this->data):?>
    <!-- Blog Post Content Column -->
    <div class="col-lg-8">

        <!-- Blog Post -->

        <!-- Title -->
        <h1><?=$this->data['name']?></h1>

        <!-- Author -->
        <p class="lead">
            by <?=$this->data['user_name']?> <?=$this->data['user_surname']?>
        </p>

        <hr>

        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span>  Publikováno <?=DateTime::createFromFormat("Y-m-d H:i:s", $this->data['created'])->format("d.m.Y H:i")?></p>
        <!-- Preview Image -->
        <?if($this->data['photo']):?>
            <hr>
                <img width="304" height="236" class="img-thumbnail" src="../../img/upload/<?=$this->data['photo']?>"/>
            <hr>
        <?endif?>

        <!-- Post Content -->
        <p class="lead"><?=$this->data['description']?></p>
        <hr>

        <!-- Blog Comments -->

        <!-- Comments Form -->

    <?else:?>
        <p>Článek nebyl nalezen</p>
    <?endif?>
</div>
<!-- /.row -->