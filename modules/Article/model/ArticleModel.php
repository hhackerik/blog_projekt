<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Detail model
 */

class ArticleModel
{
    /**Return all articles if is user logged show articles which are just for logged users
     * @return Array()
     */
    function getAllArticles()
    {

        return Db::getAllRows("SELECT a.id, a.name, a.description, a.photo, a.is_public, a.created, u.name as user_name, u.surname as user_surname
                                FROM articles as a
                                JOIN users as u on u.id = a.users_id");
    }


    function getArticle($id,$is_logged = 0)
    {

        $params = array($id);
        return Db::getOneRow("SELECT a.name, a.description, a.photo, a.is_public, a.created, u.name as user_name, u.surname as user_surname
                                FROM articles as a
                                JOIN users as u on u.id = a.users_id
                                WHERE a.id = ?",$params);

    }
}