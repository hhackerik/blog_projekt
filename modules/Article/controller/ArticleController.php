<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Get all articles for admin users
 */
        
class ArticleController extends Controller
{
    public function run()
    {
        //delete article
        if($delete_id = htmlspecialchars($_GET['delete_id']))
        {
            Db::delete("DELETE FROM articles WHERE id = ?", array($delete_id));
            Header("Location: /article");
        }

        $article_model = new ArticleModel();

        $this->data = $article_model->getAllArticles();

        $this->folder = "Article";

        $this->template = "Articles";
    }
}
?>
        