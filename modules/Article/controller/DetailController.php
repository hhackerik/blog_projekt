<?php
/**
* @author Viktor Beránek
* @author Viktor Beránek <hhackerik@me.com>
*/


/** Detail model
*/

class DetailController extends Controller
{
    public function run()
    {


        $id = htmlspecialchars($_GET['id']);

        if($id)
        {
            $detail_model = new ArticleModel();
            $this->data = $detail_model->getArticle($id);
        }
        else
        {
            $this->data = array();
        }


        $this->folder = "Article";

        $this->template = "Detail";
    }
}

        