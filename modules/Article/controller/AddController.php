<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Add article controller
 */
class AddController extends Controller
{
     public function run()
     {
        //form action
         
        if(isset($_POST['add']))
        {
            $data = $_POST['data'];
            $file = $_FILES['image'];
            try
            {
                if($data['name'] AND $data['description'])
                {
                    if($file['error'] != 0) throw new Exception("Chyba v nahrávní obrázku zkuste to znovu prosím");

                    move_uploaded_file($file['tmp_name'], "./img/upload/".$file['name']);


                    $data['users_id'] = $_SESSION['user']['id'];
                    $data['photo'] = $file['name'];
                    Db::insert("articles", $data);

                    throw new Exception("Článek přidán");

                }
                else
                {
                    throw new Exception("Vyplňte všechny políčka");
                }
            }
            catch(Exception $e)
            {
                $this->message = $e->getMessage();
            }
        }


         $this->folder = "Article";

         $this->template = "Add";
     }
}

        