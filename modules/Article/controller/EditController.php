<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Edit controller for admin users
 */

class EditController extends Controller
{
     public function run()
     {
        $id = htmlspecialchars($_GET['id']);
        $article_model = new ArticleModel();


        if($id)
        {
            if(isset($_POST['edit']))
            {
                $data = $_POST['data'];

                $file = $_FILES['image'];
               
                try
                {
                    if($data['name'] AND $data['description'])
                    {
                        if($file['size'])
                        {
                            move_uploaded_file($file['tmp_name'], "./img/upload/".$file['name']);
                            $data['photo'] = $file['name'];
                        }

                        if(!$data['is_public'])
                        {
                            $data['is_public'] = 0;
                        }




                        Db::edit("articles", $data, "WHERE id = {$id}");

                    }
                    else
                    {
                        throw new Exception("Vyplňte všechny políčka");
                    }
                }
                catch(Exception $e)
                {
                    $this->message = $e->getMessage();
                }


            }

            $this->data = $article_model->getArticle($id);
        }


         $this->folder = "Article";

         $this->template = "Edit";
     }
}

        