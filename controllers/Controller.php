<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Head class Controller from which it inherits each class
 */
abstract class Controller
{
	/** @var string $template help variable which set name of template*/
	public $template = "";

	/** @var string $folder help variable which set folder where is module in modules folder */
    protected $folder = "";

	/** Render method which is called in head layout */
    public function renderView()
    {

        if ($this->template)
        {

			if(file_exists(("templates/" . $this->template . ".php")))
			{
				require("templates/" . $this->template . ".php");
			}
			elseif(file_exists(("modules/" . $this->folder . "/templates/" . $this->template . ".php")))
			{
				require("modules/" . $this->folder . "/templates/" . $this->template . ".php");
			}
			else
			{
				require("templates/404.php");
			}
		}
		else
		{
			require("templates/404.php");
		}
	}

	/** Work method which is in each controller*/
	abstract function run();
}


    
