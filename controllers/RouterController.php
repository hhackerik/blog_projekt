<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/**
 * Class RouterController is special type of controller which by URL call right controller
 */
class RouterController extends Controller
{
	/** method parseUrl parse website URL to array
	 *	@return Array
	 */
	function parseUrl()
	{
		$url = parse_url("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$help_parsed_url = explode("/",$url['path']);

		$key_number = 0;

		for ($i = 0; $i < count($help_parsed_url); $i++)
		{
			if(!empty($help_parsed_url[$i]))
			{
				$parsed_url[$key_number] = $help_parsed_url[$i];
				$key_number++;
			}
		}

		return $parsed_url;
	}
	/** SING UP METHOD*/
	public function singUp()
	{
		//Check if user send email and password from form
		if($_POST['login']['email'] AND $_POST['login']['pswd'])
		{

			$email = $_POST['login']['email'];
			$pw = md5($_POST['login']['pswd']);

			$param_array = array($email,$pw);

			//Check if user exist
			if($logged_data = Db::getOneRow("SELECT name, surname, id, role FROM users
								WHERE email = ? AND password = ?", $param_array))
			{

				$login = array();
				$login['id'] = $logged_data['id'];
				$login['name'] = $logged_data['name'];
				$login['surname'] = $logged_data['surname'];
				$login['role'] = $logged_data['role'];
				$login['is_logged'] = 1;

				$_SESSION['user'] = $login;

			}
		}
	}

	public function logOut()
	{
		unset($_SESSION['user']);
		Header("Location: /");
	}


	
    /** Head method which call right controller */
    public function run()
    {
	
		if(isset($_POST['singup']))
		{

			$this->singUp();
		}

		if(isset($_GET['logout']))
		{
			$this->logOut();
		}

		// call class method parseUrl
		$parsed_url = $this->parseUrl();


		$name_of_class = ucfirst($parsed_url[0]);
		$name_of_controller = $name_of_class."Controller";

		if($parsed_url[1]) $name_of_subaction = ucfirst($parsed_url[1])."Controller";



		//Check if is call any Controller
		if($name_of_class)
		{

			//Check if file exists in controllers
			if (file_exists('controllers/' . $name_of_controller . '.php'))
			{
				$this->controller = new $name_of_controller();
			}
			//elseif file exists in modules

			elseif(file_exists('modules/'.$name_of_class.'/controller/' . $name_of_controller . '.php'))
			{

				if(file_exists('modules/'.$name_of_class.'/controller/'.$name_of_subaction.'.php'))
				{
					$this->controller = new $name_of_subaction();
				}
				else
				{
					$this->controller = new $name_of_controller();
				}
			}
			else
			{
				$this->controller = new Error404Controller();
			}

		}
		//else call HomepageController
		else
		{

			$this->controller = new HomepageController();

		}



		//Call method run in selected controller
		$this->controller->run();
				
		$this->template = 'layout';
    }

}