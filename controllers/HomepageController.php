<?php
/**
 * @author Viktor Beránek
 * @author Viktor Beránek <hhackerik@me.com>
 */


/** Homepage controller
 */
class HomepageController extends Controller
{
    public function run()
    {
        $homepage_model = new HomepageModel();


        $this->all_articles = $homepage_model->getAllArticles();



        $this->template = "headContainer";
    }
}